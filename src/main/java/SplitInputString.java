public class SplitInputString {
    private String value;
    private int count;

    public SplitInputString(String w, int i){
        this.value =w;
        this.count =i;
    }


    public String getValue() {
        return this.value;
    }

    public int getCount() {
        return this.count;
    }


}
