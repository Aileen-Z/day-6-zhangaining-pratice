import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String S = "\\s+";

    public String getWordFrequency(String inputStr){

        if (inputStr.split(S).length==1) {
            return inputStr + " 1";
        } else {
            try {
                List<SplitInputString> splitInputStringList = splitInputStringWith1(inputStr);

                Map<String, List<SplitInputString>> map =getListMap(splitInputStringList);

                splitInputStringList = createMapForSizingTheSameWord(map);

                return createResultString(splitInputStringList);

            } catch (Exception e) {
                return "Calculate Error";
            }
        }
    }

    public List<SplitInputString> splitInputStringWith1(String inputStr){
        String[] splitArray = inputStr.split(S);

        List<SplitInputString> splitInputStringList = new ArrayList<>();
        for (String s : splitArray) {
            SplitInputString splitInputString = new SplitInputString(s, 1);
            splitInputStringList.add(splitInputString);
        }
        return splitInputStringList;
    }

    public List<SplitInputString> createMapForSizingTheSameWord(Map<String, List<SplitInputString>> map){

        List<SplitInputString> list = new ArrayList<>();
        map.forEach((key, value) -> {
            SplitInputString splitInputString = new SplitInputString(key, value.size());
            list.add(splitInputString);
        });

        list.sort((w1, w2) -> w2.getCount() - w1.getCount());

        return list;
    }

    private String createResultString(List<SplitInputString> inputStringList){
        return inputStringList.stream()
                .map(input -> input.getValue() + " " + input.getCount())
                .collect(Collectors.joining("\n"));
    }

    private Map<String,List<SplitInputString>> getListMap(List<SplitInputString> splitInputStringList) {
        Map<String, List<SplitInputString>> map = new HashMap<>();
        for (SplitInputString splitInputString : splitInputStringList){
            if (!map.containsKey(splitInputString.getValue())){
                ArrayList arr = new ArrayList<>();
                arr.add(splitInputString);
                map.put(splitInputString.getValue(), arr);
            }

            else {
                map.get(splitInputString.getValue()).add(splitInputString);
            }
        }
        return map;
    }


}
