## 1、Objective
1 Strategy pattern, Command pattern, Observer pattern\
We learned about the three design patterns in terms of definitions, usage scenarios, differences, etc.\
2  refactor\
2.1 Code smell: Code smell is not bugs or technically incorrect. There are some common codes smell：mysterious name, duplicated code, long function(>15 rows), long parameter list, feature envy, primitive obsession, loops, temporary field, comments.\
2.2 Refactoring Demo: \
(1) Test Protection: Run test, ensure all tests pass. If no test, create test first. \ (2) find code smell \ (3) Refactor and make sure all tests pass \ (4) Baby step: Do refactor from easy to complex. Refactor one by one. One refactor with one commit. \
2.3 Some Understandings: The idea of the Split Length Method is to make sense of each step and abstract the modules for splitting. Renaming is also a process of understanding the code.

## 2、Reflective
useful

## 3、Interpretive
In the process of refactoring my code, I learned more about programming standards and how to think in a way that makes my code more readable.

## 4、Decisional
Understanding design patterns and applying them skillfully is difficult。
